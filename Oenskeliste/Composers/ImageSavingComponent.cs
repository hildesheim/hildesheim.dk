﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web.WebPages;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Events;
using Umbraco.Core.Services;
using Umbraco.Core.Services.Implement;

namespace Oenskeliste.Composers
{
    public class ImageSavingComponent : IComponent
    {
        private readonly IMediaService _mediaService;
        private readonly IContentTypeBaseServiceProvider _contentTypeBaseServiceProvider;

        public ImageSavingComponent(IMediaService mediaService,
            IContentTypeBaseServiceProvider contentTypeBaseServiceProvider)
        {
            _mediaService = mediaService;
            _contentTypeBaseServiceProvider = contentTypeBaseServiceProvider;
        }

        public void Initialize()
        {
            ContentService.Saving += ContentService_Saving;
        }

        public void Terminate()
        {
            ContentService.Saving -= ContentService_Saving;
        }

        private void ContentService_Saving(IContentService sender, ContentSavingEventArgs e)
        {
            foreach (var content in e.SavedEntities.Where(c => c.ContentType.Alias.InvariantEquals("wishList")))
            {
                if (content?.GetValue<string>("wishes") == null)
                {
                    return;
                }

                var imageFolderId = content.GetValue<int>("imageFolderId") != 0 ? content.GetValue<int>("imageFolderId") : 1114;
                
                List<Dictionary<string, object>> wishes =
                    JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(content.GetValue<string>("wishes"));
                foreach (Dictionary<string, object> wish in wishes)
                {
                    var umbracoImage = wish["image"];

                    if (umbracoImage != null)
                    {
                        continue;
                    }

                    var newImage = wish["newImageUrl"];
                    if (newImage == null)
                    {
                        continue;
                    }

                    var newImageUrl = newImage.ToString();
                    var imageUrlHasJpg = newImageUrl.Contains(".jpg");
                    var imageUrlHasPng = newImageUrl.Contains(".png");
                    var fileExtension = string.Empty;
                    if (imageUrlHasJpg)
                    {
                        fileExtension = ".jpg";
                    }
                    else if (imageUrlHasPng)
                    {
                        fileExtension = ".png";
                    }

                    if (fileExtension.IsEmpty())
                    {
                        continue;
                    }

                    var title = wish["title"].ToString();
                    var titleWithExtension = $"{title}{fileExtension}";
                    
                    using (var client = new HttpClient())
                    {
                        using (HttpResponseMessage response = client.GetAsync(newImageUrl).Result)
                        {
                            var imageData = response.Content.ReadAsStreamAsync().Result;

                            // var data = client.GetStreamAsync(newImageUrl).Result;
                            var mediaImage = _mediaService.CreateMedia(title, imageFolderId, "Image");
                            mediaImage.SetValue(_contentTypeBaseServiceProvider, "umbracoFile", titleWithExtension,
                                imageData);
                            
                            var imageSaveResult= _mediaService.Save(mediaImage);
                            if (imageSaveResult.Success)
                            {
                                
                                wish["newImageUrl"] = string.Empty;
                                wish["image"] = mediaImage.GetUdi();
                                wish["itemType"] = "test";
                                content.SetValue("wishes", JsonConvert.SerializeObject(wishes));
                                
                                sender.SaveAndPublish(content, "*", -1, false);
                            }
                        }
                    }
                }
            }
        }
    }
}