(function () {
  const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

  let day1 = "March 31, 2021 08:00:00",
      day2 = "April 28, 2021 08:00:00",
      countDown1 = new Date(day1).getTime(),
      countDown2 = new Date(day2).getTime(),
      x = setInterval(function() {    

        let now = new Date().getTime(),
            distance1 = countDown1 - now,
            distance2 = countDown2 - now;

            document.getElementById("days_1").innerText = Math.floor(distance1 / (day)),
            document.getElementById("hours_1").innerText = Math.floor((distance1 % (day)) / (hour)),
            document.getElementById("minutes_1").innerText = Math.floor((distance1 % (hour)) / (minute)),
            document.getElementById("seconds_1").innerText = Math.floor((distance1 % (minute)) / second);

        document.getElementById("days").innerText = Math.floor(distance2 / (day)),
          document.getElementById("hours").innerText = Math.floor((distance2 % (day)) / (hour)),
          document.getElementById("minutes").innerText = Math.floor((distance2 % (hour)) / (minute)),
          document.getElementById("seconds").innerText = Math.floor((distance2 % (minute)) / second);



        //do something later when date is reached
        if (distance1 < 0) {
          let headline = document.getElementById("headline_1"),
              countdown = document.getElementById("countdown_1"),
              content = document.getElementById("content_1");

          headline.innerText = "Påske!!";
          countdown.style.display = "none";
          content.style.display = "block";

          clearInterval(x);
        }

        if (distance2 < 0) {
            let headline = document.getElementById("headline"),
                countdown = document.getElementById("countdown"),
                content = document.getElementById("content");
  
            headline.innerText = "Sommerhus!!";
            countdown.style.display = "none";
            content.style.display = "block";
  
            clearInterval(x);
          }
        //seconds
      }, 0)
  }());